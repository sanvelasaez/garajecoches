package com.curso.ejercicio.garaje.entity;

import com.curso.ejercicio.garaje.service.Conducible;

/**
 * 
 * Clase padre de todos los vehiculos
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public abstract class Vehiculo implements Conducible {

	/**
	 * Enumeracion de los tipos de vehiculos y sus respectivos datos no variables
	 */
	public enum tipoVehiculo {

		/**
		 * Tipo Coche (4 ruedas, 220 km/h)
		 */
		COCHE(4, 220),
		/**
		 * Tipo Camion (6 ruedas, 180 km/h)
		 */
		CAMION(6, 180),
		/**
		 * Tipo Moto (2 ruedas, 250 km/h)
		 */
		MOTO(2, 250),
		/**
		 * Tipo Bici (2 ruedas, 50 km/h)
		 */
		BICI(2, 50),
		/**
		 * Tipo Barco (0 ruedas, 120 km/h)
		 */
		BARCO(0, 120);

		private final int NUM_RUEDAS;
		private final int VEL_MAX;

		/**
		 * 
		 * @param NUM_RUEDAS. Numero de ruedas del vehiculo
		 * @param VEL_MAX.    Velocidad maxima del vehiculo
		 */
		tipoVehiculo(int NUM_RUEDAS, int VEL_MAX) {
			this.NUM_RUEDAS = NUM_RUEDAS;
			this.VEL_MAX = VEL_MAX;
		}

		/**
		 * 
		 * @return NUM_RUEDAS. Devuelve el numero de ruedas del vehiculo
		 */
		public int getNumRuedas() {
			return this.NUM_RUEDAS;
		}

		/**
		 * 
		 * @return VEL_MAX. Devuelve la velocidad maxima a la que puede ir el vehiculo
		 */
		public int getVelMax() {
			return this.VEL_MAX;
		}
	}

	private String color;
	private tipoVehiculo tipoVehiculo;

	private double velocidad;
	private double horasRecorridas;
	private double distanciaRecorrida;

	/**
	 * Constructor de la clase Vehiculo.
	 * 
	 * Asigna por defecto a 0 las variables de velocidad, horasRecorridas y
	 * distancia Recorrida.
	 * 
	 * @param tipoVehiculo Parametro que asigna el tipo de vehiculo que es, dandole
	 *                     por defecto su numero de ruedas y la velocidad maxima a
	 *                     la que puede llegar.
	 * 
	 * @param color.       Parametro para asignar un color al vehiculo
	 */
	protected Vehiculo(tipoVehiculo tipoVehiculo, String color) {
		this.tipoVehiculo = tipoVehiculo;
		this.color = color;
		this.velocidad = 0;
		this.horasRecorridas = 0;
		this.distanciaRecorrida = 0;
	}

	/**
	 * Devuelve el tipo de vehiculo
	 * 
	 * @return tipoVehiculo
	 */
	public tipoVehiculo getTipoVehiculo() {
		return tipoVehiculo;
	}

	/**
	 * Devuelve el color del vehiculo
	 * 
	 * @return color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Asigna un nuevo color al vehiculo
	 * 
	 * @param color del vehiculo
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * Devuelve la velocidad a la que va el vehiculo
	 * 
	 * @return velocidad
	 */
	public double getVelocidad() {
		return velocidad;
	}

	/**
	 * Asigna la velocidad a la que va a ir el vehiculo
	 * 
	 * @param velocidad en Km/h
	 */
	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	/**
	 * Devuelve las horas recorridas durante el trayecto
	 * 
	 * @return horasRecorridas
	 */
	public double getHorasRecorridas() {
		return horasRecorridas;
	}

	/**
	 * Actualiza el total de las horas recorridas durante el trayecto
	 * 
	 * @param horasRecorridas del trayecto
	 */
	public void addHorasRecorridas(double horasRecorridas) {
		this.horasRecorridas += horasRecorridas;
	}

	/**
	 * Devuelve las distancia recorrida (Km) durante el trayecto
	 * 
	 * @return distanciaRecorrida.
	 */
	public double getDistanciaRecorrida() {
		return distanciaRecorrida;
	}

	/**
	 * Actualiza el total de la distancia recorrida (Km) durante el trayecto
	 * 
	 * @param distanciaRecorrida del trayecto
	 */
	public void addDistanciaRecorrida(double distanciaRecorrida) {
		this.distanciaRecorrida += distanciaRecorrida;
	}

	@Override
	public String toString() {
		return "\nTipo de vehiculo: " + tipoVehiculo + "\nColor: " + color + "\nNumero de ruedas "
				+ tipoVehiculo.getNumRuedas();
	}

	@Override
	public void conducir() {
		System.out.println("Se ha montado en su vehiculo");
		this.horasRecorridas = 0;
		this.distanciaRecorrida = 0;
	}

	@Override
	public void avanzar(double distancia) {
		double tiempo = distancia / getVelocidad();
		addDistanciaRecorrida(distancia);
		addHorasRecorridas(tiempo);
		System.out.println("Ha avanzado " + distancia + " Km en " + tiempo + " horas");
	}

	@Override
	public void retroceder(double distancia) {
		double tiempo = distancia / getVelocidad();
		addDistanciaRecorrida(-distancia);
		addHorasRecorridas(tiempo);
		System.out.println("Ha avanzado " + distancia + " Km en " + tiempo + " horas");
	}

	@Override
	public void parar() {
		System.out.println("Ya no esta avanzando");
		System.out.println(dameEstado());
	}

	/**
	 * Devuelve el estado del trayecto (tiempo y distancia) del vehiculo
	 * seleccionado
	 * 
	 * @return String de la actualizacion del estado del trayecto
	 */
	public String dameEstado() {
		return "Lleva recorrido con su vehiculo " + getDistanciaRecorrida() + " Km y " + getHorasRecorridas()
				+ " horas";
	}

}
