package com.curso.ejercicio.garaje.entity;

/**
 * 
 * Clase abstracta hija de la clase Vehiculo para que la hereden los vehiculos a
 * motor.
 * 
 * Los vehiculos que posean motor heredaran de esta clase.
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public abstract class VehiculoMotor extends Vehiculo {

	private String matricula;

	/**
	 * Constructor de la clase VehiculoMotor.
	 * 
	 * @param tipoVehiculo. El tipo de vehiculo que es
	 * @param color.        El color del vehiculo
	 * @param matricula.    La matricula u otro codigo de identificacion del
	 *                      vehiculo
	 */
	protected VehiculoMotor(tipoVehiculo tipoVehiculo, String color, String matricula) {
		super(tipoVehiculo, color);
		this.matricula = matricula;
	}

	/**
	 * Devuelve la matricula o codigo de identificacion del vehiculo
	 * 
	 * @return matricula
	 */
	public String getMatricula() {
		return matricula;
	}

	// Se une al toString de la clase padre vehiculo
	@Override
	public String toString() {
		return "\nMatricula: " + matricula + super.toString();
	}

	// Como es un vehiculo a motor tiene que arrancarse tambien
	@Override
	public void conducir() {
		super.conducir();
		System.out.println("Motor arrancado");
	}

	// Como es un vehiculo a motor tiene que apagarse tambien
	@Override
	public void parar() {
		System.out.println("Motor apagado");
		super.parar();
	}

}
