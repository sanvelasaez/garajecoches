package com.curso.ejercicio.garaje.entity;

/**
 * Clase Barco heredada de VehiculoMotor.
 * 
 * Instanciar la clase Barco para ser usada
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public class Barco extends VehiculoMotor {

	/**
	 * Constructor de la clase Barco
	 * 
	 * @param color     del vehiculo
	 * @param matricula del vehiculo
	 */
	public Barco(String color, String matricula) {
		super(tipoVehiculo.BARCO, color, matricula);
	}

}
