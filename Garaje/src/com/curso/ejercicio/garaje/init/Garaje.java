package com.curso.ejercicio.garaje.init;

import java.util.ArrayList;
import java.util.Scanner;

import com.curso.ejercicio.garaje.entity.Barco;
import com.curso.ejercicio.garaje.entity.Bici;
import com.curso.ejercicio.garaje.entity.Camion;
import com.curso.ejercicio.garaje.entity.Coche;
import com.curso.ejercicio.garaje.entity.Moto;
import com.curso.ejercicio.garaje.entity.Vehiculo;

/**
 * 
 * Proyecto que consiste en la organizacion de un garaje con una serie de
 * vehiculos en el que se pueden registrar nuevos vehiculos, listarlos o usarlos
 * 
 * Las clases de vehiculos que han sido implementadss como ejemplo han sido:
 * 
 * · Coche
 * 
 * · Camion (con tacometro)
 * 
 * · Moto
 * 
 * · Bici
 * 
 * · Barco
 * 
 * @version 1.1 Se agregaron una serie de nuevos vehiculos para ejemplo
 * @author Santiago Velasco
 *
 */
public class Garaje {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);// Scaner para leer datos por consola

		boolean q = true; // Variable booleana para tratar excepciones
		int tipoVehiculo; // El tipo de vehiculo a registrar
		String matricula = null; // Matricula del vehiculo
		String color = null; // Color del vehiculo
		ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>(); // Array que almacena todos los vehiculos del garaje

		/**
		 * Vehiculos de ejemplo
		 */
		vehiculos.add(new Coche("M5236XU", "gris"));
		vehiculos.add(new Camion("5236LMT", "azul"));
		vehiculos.add(new Moto("3495JTD", "rojo"));
		vehiculos.add(new Barco("3579LJF", "negro"));
		vehiculos.add(new Coche("8452RND", "blanco"));
		vehiculos.add(new Camion("6482BRS", "verde"));
		vehiculos.add(new Bici("violeta"));

		System.out.println("Bienvenido al garaje, ¿que desea hacer?");

		// Bucle que da inicio al menu
		do {// Inicio bucle 1
			System.out.println(
					"1 - Registrar un vehiculo\n" + "2 - Sacar uno de los vehiculos\n" + "3 - Listar los vehiculos");
			System.out.println("Puede retroceder al menu anterior en todo momento introduciendo 0");

			switch (scanner.nextInt()) { // Inicio primer switch
			case 1: {

				do {// Inicio bucle 1.1
					q = true;
					System.out.println(
							"1 - Coche\n" + "2 - Camion\n" + "3 - Motocicleta\n" + "4 - Bicicleta\n" + "5 - Barco");
					tipoVehiculo = scanner.nextInt();

					if (tipoVehiculo > 0 && tipoVehiculo < 6) {
						System.out.println("¿De que color es su vehiculo?");
						color = scanner.next();
						if (tipoVehiculo != 4) {// Si no es una bici
							System.out.println("¿Cual es su matricula?");
							matricula = scanner.next();
						}
					}

					switch (tipoVehiculo) {
					case 0: {
						q = true;
						break;
					}
					case 1: {
						vehiculos.add(new Coche(matricula, color));
						break;
					}
					case 2: {
						vehiculos.add(new Camion(matricula, color));
						break;
					}
					case 3: {
						vehiculos.add(new Moto(matricula, color));
						break;
					}
					case 4: {
						vehiculos.add(new Bici(color));
						break;
					}
					case 5: {
						vehiculos.add(new Barco(matricula, color));
						break;
					}
					default:
						System.out.println("Vehiculo no registrado introduzca uno existente\n");
						q = false;
					}

				} while (!q); // Final bucle 1.1

				System.out.println("Usted ha registrado el siguiente vehiculo");
				System.out.println(vehiculos.get(vehiculos.size() - 1).toString());

				break;
			}

			case 2: {

				do {// Inicio bucle 1.2

					System.out.println("¿Que vehiculo desea sacar?\nIntroduzca el ID asignado al vehiculo");
					int idVehiculo = 0; // ID del vehiculo seleccionado
					Vehiculo vehiculoSeleccionado; // Vehiculo seleccionado

					do {// Inicio bucle 1.2.1
						q = true;
						idVehiculo = scanner.nextInt();
						if (idVehiculo < 0 || idVehiculo > vehiculos.size()) {// Si existe el vehiculo
							System.out.println("Vehiculo no encontrado");
							System.out.println("Introduzca un ID valido o un 0 para salir");
							q = false;
						}
					} while (!q && idVehiculo != 0);// Final bucle 1.2.1

					vehiculoSeleccionado = vehiculos.get(idVehiculo - 1);// Obtiene el vehiculo seleccionado
					System.out.println("Ha elegido el vehiculo " + vehiculoSeleccionado.toString());

					System.out.println("¿Desea conducir el vehiculo seleccionado? si/no");

					if (scanner.next().equalsIgnoreCase("si")) {// Si confirma la seleccion del vehiculo

						vehiculoSeleccionado.conducir();// Inicio del trayecto

						int opcion;
						double velocidad = 0;
						boolean parado = false;

						do {// Inicio bucle 1.2.2

							System.out.println("¿Que desea hacer ahora?");

							do {// Inicio bucle 1.2.2.1
								q = true;
								System.out.println("1 - Avanzar\n" + "2 - Retroceder\n" + "3 - Parar y salir\n");
								opcion = scanner.nextInt();

								switch (opcion) {// Inicio segundo switch
								case 0: {
									q = parado;
									if (!q) // Si no se ha parado no se puede finalizar
										System.out.println("Debe parar el coche antes");
									break;
								}
								case 1: {
									System.out.println("¿A que velocidad desea avanzar? (Km/h)");
									if ((velocidad = scanner.nextDouble()) < vehiculoSeleccionado.getTipoVehiculo()
											.getVelMax()) {// Comprueba que el vehiculo no supera la velocidad maxima

										vehiculoSeleccionado.setVelocidad(velocidad);// Asigna la velocidad

										// Asigna los Km que se avanzaran
										System.out.println("¿Durante cuantos kilometros?");
										vehiculoSeleccionado.avanzar(scanner.nextDouble());

										// Si es un camion se agrega la velocidad a su tacometro
										if (vehiculoSeleccionado.getClass().equals(Camion.class))
											((Camion) vehiculoSeleccionado).addVelocidad(velocidad);

									} else {
										System.out.println("No puede ir tan rapido con este vehiculo");
										q = false;
									}
									break;
								}
								case 2: {
									System.out.println("¿A que velocidad desea retroceder? (Km/h)");
									if ((velocidad = scanner.nextDouble()) < vehiculoSeleccionado.getTipoVehiculo()
											.getVelMax()) {// Comprueba que el vehiculo no supera la velocidad maxima

										vehiculoSeleccionado.setVelocidad(velocidad);// Asigna la velocidad

										// Asigna los Km que se retrocederan
										System.out.println("¿Durante cuantos kilometros?");
										vehiculoSeleccionado.retroceder(scanner.nextDouble());

										// Si es un camion se agrega la velocidad a su tacometro
										if (vehiculoSeleccionado.getClass().equals(Camion.class))
											((Camion) vehiculoSeleccionado).addVelocidad(velocidad);

									} else {
										System.out.println("No puede ir tan rapido con este vehiculo");
										q = false;
									}
									break;
								}
								case 3: {
									vehiculoSeleccionado.parar();// Se detiene el vehiculo y finaliza el trayecto
									parado = true;
									opcion = 0;
									break;
								}
								default:
									System.out.println("Introduzca una opción valida.");
									q = false;
								}// Final segundo switch

							} while (!q);// Final bucle 1.2.2.1

						} while (opcion != 0);// Final bucle 1.2.2

					} else {// Si se desea seleccionar otro vehiculo
						System.out.println("¿Desea seleccionar otro vehiculo? si/no");
						q = scanner.next().equalsIgnoreCase("si");
					}

				} while (!q);// Final bucle 1.2

				break;
			}

			case 3: {
				int id = 1; // Identificador para vehiculo
				for (Vehiculo vehiculo : vehiculos) {
					System.out.println("ID del vehiculo: " + id++ + vehiculo.toString());
					System.out.println("==========================================================");
				}
				break;
			}

			default:
				System.out.println("Opcion invalida");
			}// Final primer switch

			System.out.println("¿Desea hacer alguna otra gestion? si/no");

		} while (scanner.next().equalsIgnoreCase("si")); // Final bucle 1
	}
}
