# Nombre del Proyecto: Gestor de Vehículos de Garaje

Este proyecto está desarrollado en Eclipse utilizando el lenguaje de programación Java. El proyecto permite administrar los vehículos de un garaje, brindando funcionalidades como listar los vehículos, agregar nuevos vehículos y sacarlos para su uso. Al sacarlos, se pueden realizar cuatro acciones: conducir, avanzar, retroceder y detenerse. Al realizar las acciones de avanzar y retroceder, se registra el tiempo y la distancia recorrida.

## Descripción del Proyecto

El objetivo principal de este proyecto es proporcionar una herramienta para administrar los vehículos de un garaje. La aplicación ofrece las siguientes funcionalidades:

- Añadir vehículos: Los usuarios pueden agregar nuevos vehículos al garaje, proporcionando información como el tipo de vehículo, matricula (si tiene) y color.
- Listar vehículos: Permite ver la lista de todos los vehículos registrados en el garaje.
- Sacar vehículos para su uso: Los usuarios pueden seleccionar un vehículo y realizar las siguientes acciones con él:
    - Conducir: Permite montarse en el vehículo y comenzar el trayecto. En el caso de ser a motor también se arranca.
    - Avanzar: Registra el tiempo y la distancia recorrida cuando el vehículo avanza.
    - Retroceder: Tiene la misma función que avanzar pero disminuyendo la distancia total ya que se está yendo en dirección contraria.
    - Parar: Detiene el vehículo y finaliza el trayecto. Si no se ejecuta no se pueden hacer otras gestiones.

La arquitectura del proyecto se basa en una clase padre "Vehiculo" y una interfaz "Conducible". La clase "Vehiculo" es la clase base de la cual heredan todos los tipos de vehículos, y la interfaz "Conducible" define los métodos para conducir, avanzar, retroceder y parar. Además, existe una subclase de "Vehiculo" llamada "VehiculoMotor" que hereda de ella y se utiliza para los vehículos con motor.

## Nuevas Implementaciones

Las innovaciones que se han desarrollado respecto a la base del proyecto han sido las siguientes:
- Se le ha añadido a la clase Camión la propiedad tacómetro para almacenar todas las velocidades a las ha ido durante todo su trayecto.

## Tecnologías Utilizadas

El proyecto ha sido desarrollado utilizando las siguientes tecnologías y herramientas:

- Java: Lenguaje de programación utilizado para implementar la lógica del proyecto.
- Eclipse: Entorno de desarrollo integrado (IDE) utilizado para desarrollar y administrar el proyecto.

## Configuración del Proyecto

Para configurar y ejecutar el proyecto en tu entorno local, sigue los pasos a continuación:

1. Clona este repositorio en tu máquina local o descárgalo como archivo ZIP.
2. Abre el proyecto en Eclipse o importa el proyecto en tu IDE preferido.
3. Compila el proyecto y asegúrate de que no haya errores.
4. Ejecuta el programa desde la clase principal o el punto de entrada definido.

## Contribución

Si deseas contribuir a este proyecto, puedes seguir los pasos a continuación:

1. Realiza un fork de este repositorio y clónalo en tu máquina local.
2. Crea una rama nueva para realizar tus modificaciones: `git checkout -b feature/nueva-caracteristica`.
3. Realiza las modificaciones y realiza confirmaciones (commits) claros para describir tus cambios.
4. Publica tus cambios en tu repositorio remoto: `git push origin feature/nueva-caracteristica`.
5. Crea una solicitud de extracción (pull request) en este repositorio y describe tus modificaciones detalladamente.

Todas las contribuciones son bienvenidas. Cualquier corrección de errores, mejoras de funcionalidad o nuevas características serán apreciadas.

## Contacto

Si tienes alguna pregunta, sugerencia o problema relacionado con el proyecto, no dudes en contactar al equipo de desarrollo a través del siguiente correo electrónico: [micorreo@example.com](mailto:micorreo@example.com).
